#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_lbText = new QLabel(this);
    m_lbText->setText("Текст!");
    m_lbText->move(0, 0);
    m_lbText->hide();

    m_pbShowText = new QPushButton(this);
    m_pbShowText->setText("Показать текст");
    m_pbShowText->move(50, 50);

//    connect(m_pbShowText, SIGNAL(clicked(bool)), m_lbText, SLOT(show()));
    connect(m_pbShowText, SIGNAL(clicked(bool)), this, SLOT(showText()));
}

MainWindow::~MainWindow()
{

}

void MainWindow::showText()
{
    m_lbText->show();
}



