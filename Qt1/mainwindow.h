#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QPushButton* m_pbShowText;
    QLabel* m_lbText;

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void showText();

};

#endif // MAINWINDOW_H


