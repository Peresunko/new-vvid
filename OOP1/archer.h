#ifndef ARCHER_H
#define ARCHER_H

#include "warrior.h"

class Archer : public Warrior
{
    int m_arrowsCount;
public:
    Archer(QString name, int hp, int arrowsCount);

    void setArrowsCount(int count);
    int getArrowsCount();
    void showReport();
};

#endif // ARCHER_H

