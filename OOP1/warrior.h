#ifndef WARIOR_H
#define WARIOR_H

#include <QString>
#include <QDebug>

class Warrior
{
    QString m_name;
    int m_currentHP;

public:
    Warrior(QString name, int hp);
    void showReport();

    void setHP(int hp);
    void setName(QString name);

    int getHP();
    QString getName();
};

#endif // WARIOR_H











