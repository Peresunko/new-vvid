#include "warrior.h"

Warrior::Warrior(QString name, int hp)
{
    setName(name);
    setHP(hp);
}

void Warrior::showReport()
{
    QString str = QString("Hello! I am warrior. My name is ")
            + getName() + QString(". My current HP is ")
            + QString::number(getHP());

    qDebug() << str;
}

void Warrior::setHP(int hp)
{
    if (hp < 0)
        hp = 0;
    if (hp > 100)
        hp = 100;
    m_currentHP = hp;
}

void Warrior::setName(QString name)
{
    m_name = name;
}

int Warrior::getHP()
{
    return m_currentHP;
}

QString Warrior::getName()
{
    return m_name;
}


