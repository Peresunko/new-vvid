#include "archer.h"

Archer::Archer(QString name, int hp, int arrowsCount) : Warrior(name, hp)
{
    setArrowsCount(arrowsCount);
}

void Archer::setArrowsCount(int count)
{
    if (count < 0)
        count = 0;
    m_arrowsCount = count;
}

int Archer::getArrowsCount()
{
    return m_arrowsCount;
}

void Archer::showReport()
{
    QString str = QString("Hello! I am archer. My name is ") + getName()
            + QString(". My current HP is ") + QString::number(getHP())
            + QString(". My arrows count is ") + QString::number(getArrowsCount());

    qDebug() << str;
}
