#ifndef TEST_MONEYTEST_H
#define TEST_MONEYTEST_H

#include <QtCore>
#include <QtTest/QtTest>

class Test_MoneyTest : public QObject
{
    Q_OBJECT

public:
    Test_MoneyTest();

private slots:
    void testMultiplication();
    void testFrancMultiplication();
    void testEquality();
    void checkConstructionWithoudNew();
    void testFrancEquality();
};

#endif // TEST_MONEYTEST_H
