#include <QtTest>
#include "../dollar.h"
#include "../franc.h"
#include "../money.h"
#include "test_moneytest.h"

Test_MoneyTest::Test_MoneyTest()
{
}

void Test_MoneyTest::testMultiplication()
{
    Dollar five(5);
    Dollar product = five.times(2);
    Dollar product10(10);
    QVERIFY(product.equals(product10));

    product = five.times(3);
    Dollar product15(15);
    QVERIFY(product.equals(product15));
}

void Test_MoneyTest::testFrancMultiplication()
{
    Franc five(5);
    Franc product = five.times(2);
    Franc product10(10);
    QVERIFY(product.equals(product10));

    product = five.times(3);
    Franc product15(15);
    QVERIFY(product.equals(product15));
}

void Test_MoneyTest::testEquality()
{
    Dollar five1(5);
    Dollar five2(5);
    QVERIFY(five1.equals(five2));
}

void Test_MoneyTest::testFrancEquality()
{
    Franc five1(5);
    Franc five2(5);
    QVERIFY(five1.equals(five2));

    Franc six(6);
    QVERIFY(!five1.equals(six));
}

void Test_MoneyTest::checkConstructionWithoudNew()
{
    Dollar five1(5);
    QVERIFY(Dollar(5).equals(five1));
}
