#ifndef FRANC_H
#define FRANC_H

#include "money.h"

class Franc : public Money
{
public:
    Franc(int amount);
    Franc times(int multiplier);
};


#endif // FRANC_H

