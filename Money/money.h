#ifndef MONEY_H
#define MONEY_H

class Money
{
protected:
    int m_amount;
public:
    Money(int amount);
    bool equals(const Money& m);
};

#endif // MONEY_H
