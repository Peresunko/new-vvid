#ifndef DOLLAR_H
#define DOLLAR_H

#include "money.h"

class Dollar : public Money
{
public:
    Dollar(int amount);
    Dollar times(int multiplier);
};

#endif // DOLLAR_H
