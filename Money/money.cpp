#include "money.h"

Money::Money(int amount)
{
    m_amount = amount;
}

bool Money::equals(const Money& m)
{
    return m.m_amount == m_amount;
}
